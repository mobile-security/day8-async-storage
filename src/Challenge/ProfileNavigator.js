import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Button,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ProfileInput from './ProfileInput';

const ProfileNavigator = () => {
  const [identitas, setIdentitas] = useState(null);
  const [showInput, setShowInput] = useState(false);

  const getProfile = async () => {
    const jsonValue = await AsyncStorage.getItem('@profile');
    setIdentitas(jsonValue != null ? JSON.parse(jsonValue) : null);
  };

  const saveProfile = async newProfile => {
    await AsyncStorage.setItem('@profile', JSON.stringify(newProfile));
    setIdentitas(newProfile);
    setShowInput(false);
  };

  useEffect(() => {
    getProfile();
  }, []);

  return (
    <SafeAreaView style={styles.canvas}>
      <View>
        {showInput ? (
          <ProfileInput onSave={saveProfile} />
        ) : (
          <View>
            {identitas ? (
              <View style={styles.box}>
                <View>
                  <Text style={styles.textnama}>Nama: {identitas.Nama}</Text>
                  <Text style={styles.textumur}>Umur: {identitas.Umur}</Text>
                  <Text style={styles.textemail}>Email: {identitas.Email}</Text>
                  <Text style={styles.textnohp}>
                    Nomor Hp: {identitas.NomorHP}
                  </Text>
                  <Text style={styles.textalamat}>
                    Alamat: {identitas.Alamat}
                  </Text>
                </View>
                <TouchableOpacity
                  style={styles.buttoncreateacc}
                  onPress={() => setShowInput(true)}>
                  <Text style={styles.textcreateacc}>Create Account</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <Button
                title="Create Profile"
                onPress={() => setShowInput(true)}
              />
            )}
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  canvas: {backgroundColor: 'white', width: 420, height: 1000},
  box: {
    backgroundColor: 'brown',
    width: Platform.OS == 'ios' ? 390 : 412,
    height: 500,
    marginTop: 50,
    borderRadius: 20,
  },
  textnama: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: 20,
    fontWeight: 'bold',
  },
  textumur: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: 20,
    fontWeight: 'bold',
  },
  textemail: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: 20,
    fontWeight: 'bold',
  },
  textnohp: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: 20,
    fontWeight: 'bold',
  },
  textalamat: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: 20,
    fontWeight: 'bold',
  },
  buttoncreateacc: {
    backgroundColor: 'gray',
    borderRadius: 20,
    width: 270,
    height: 40,
    overflow: 'hidden',
    marginTop: Platform.OS == 'ios' ? 40 : 30,
    marginLeft: Platform.OS == 'ios' ? 70 : 70,
  },
  textcreateacc: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    top: 6,
  },
});
export default ProfileNavigator;
