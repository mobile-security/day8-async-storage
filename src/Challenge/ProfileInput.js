import React, {useState} from 'react';
import {
  View,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Platform,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';

const ProfileInput = ({onSave}) => {
  const [Nama, setNama] = useState('');
  const [Umur, setUmur] = useState('');
  const [Email, setEmail] = useState('');
  const [NomorHP, setHP] = useState('');
  const [Alamat, setAlamat] = useState('');

  const handleSave = () => {
    onSave({Nama, Umur, Email, NomorHP, Alamat});
  };

  return (
    <KeyboardAvoidingView>
      <SafeAreaView style={styles.canvas}>
        <ScrollView showsVerticalScrollIndicator={true}>
          <View style={styles.boxnama}>
            <TextInput
              style={styles.textnama}
              placeholder="Nama..."
              value={Nama}
              onChangeText={setNama}
              placeholderTextColor="white"
              fontWeight="bold"
              color="black"
            />

            <TextInput
              style={styles.textumur}
              placeholder="Umur..."
              value={Umur}
              onChangeText={setUmur}
              placeholderTextColor="white"
              fontWeight="bold"
              color="black"
            />
            <TextInput
              style={styles.textemail}
              placeholder="Email..."
              value={Email}
              onChangeText={setEmail}
              placeholderTextColor="white"
              fontWeight="bold"
              color="black"
            />
            <TextInput
              style={styles.textnohp}
              placeholder="Nomor HP..."
              value={NomorHP}
              onChangeText={setHP}
              placeholderTextColor="white"
              fontWeight="bold"
              color="black"
            />
            <TextInput
              style={styles.textalamat}
              placeholder="Alamat..."
              value={Alamat}
              onChangeText={setAlamat}
              placeholderTextColor="white"
              fontWeight="bold"
              color="black"
            />
            <TouchableOpacity style={styles.buttonsave} onPress={handleSave}>
              <Text style={styles.textsave}>Save</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  canvas: {
    backgroundColor: 'white',
    width: 420,
    height: 1000,
  },
  boxnama: {
    backgroundColor: '#0E8388',
    width: Platform.OS == 'ios' ? 390 : 412,
    height: Platform.OS == 'ios' ? 500 : 430,
    marginTop: 90,
    borderRadius: 20,
  },
  textnama: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: 8,
  },
  textumur: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: Platform.OS == 'ios' ? 10 : -20,
  },
  textemail: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: Platform.OS == 'ios' ? 10 : -40,
  },
  textnohp: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: Platform.OS == 'ios' ? 10 : -55,
  },
  textalamat: {
    color: 'white',
    fontSize: 16,
    padding: 30,
    top: Platform.OS == 'ios' ? 10 : -70,
  },
  buttonsave: {
    backgroundColor: '#CBE4DE',
    borderRadius: 20,
    width: 270,
    height: 40,
    overflow: 'hidden',
    marginTop: Platform.OS == 'ios' ? 40 : -70,
    marginLeft: Platform.OS == 'ios' ? 60 : 70,
  },
  textsave: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    top: 6,
  },
});

export default ProfileInput;
