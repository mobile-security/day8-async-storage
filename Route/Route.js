import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ProfileNavigator from '../src/Challenge/ProfileNavigator';
import {View} from 'react-native/types';
import ProfileInput from '../src/Challenge/ProfileInput';

const Stack = createNativeStackNavigator();

//Router ini adalah kumpulan dari beberapa navigasi ke antar halaman
const Route = () => {
  return (
    // <View>
    //   <Stack.Navigator
    //     screenOptions={{
    //       headerShown: false,
    //     }}>
    //     <Stack.Screen name="ProfileNavigator" component={ProfileNavigator} />
    //     <Stack.Screen name="ProfileInput" component={ProfileInput} />
    //   </Stack.Navigator>
    // </View>
    // <View>
    <ProfileNavigator />
    // </View>
  );
};

export default Route;
